#!/bin/bash
# Downscale photos in the current working directory for publishing online,
# store under ./small

shopt -s nocaseglob

mkdir --parents small
for file in *.jpg; do
    convert "$file" -resize '1200x>' "small/${file%.*}.jpg"
done
