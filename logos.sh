#!/bin/bash
# Take a list of logo filenames and create properly resized and framed
# ones under ./small. If no logos specified, perform the action on all
# graphic files in the current working directory.

shopt -s nullglob

TSIZE=200x200

mkdir --parents small

if (( $# < 1 )); then
    files="*.png *.jpg *.tiff *.pdf *.eps *.svg"
else
    files="$@"
fi

for file in $files; do
    echo "Зменшую $file..."
    convert "$file" \
            -flatten -trim -resize $TSIZE \
            -background white -gravity Center -extent $TSIZE \
            "small/${file%.*}.png"
done
